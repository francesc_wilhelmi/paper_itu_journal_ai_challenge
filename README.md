# README #

### Authors
* [Francesc Wilhelmi](https://fwilhelmi.github.io/) (Universitat Pompeu Fabra)
* [Boris Bellalta](http://www.dtic.upf.edu/~bbellalt/) (Universitat Pompeu Fabra)
* David Góez (Universidad de Antioquia)
* Paola Soto (University of Antwerp)
* Ramon Vallés (Universitat Pompeu Fabra)
* Mohammad Alfaifi (Saudi Telecom)
* Abdulrahman Algunayah (Saudi Telecom)
* Jorge Martin-Pérez (Universidad Carlos III de Madrid)
* Luigi Girletti (Universidad Carlos III de Madrid)
* Rajasekar Mohan (PES University)
* K Venkat Ramnan (PES University)

### Project description
With the advent of Artificial Intelligence (AI)-empowered communications, industry, academia, and standardization organizations are progressing on the definition of mechanisms and procedures to address the increasing complexity of future 5G and beyond communications. In this context, the International Telecommunication Union (ITU) organized the first AI for 5G Challenge to bring industry and academia together to introduce and solve representative problems related to the application of Machine Learning (ML) to networks. In this paper, we present the results gathered from Problem Statement 13 (PS-013), organized by Universitat Pompeu Fabra (UPF), which primary goal was predicting the performance of next-generation Wireless Local Area Networks (WLANs) applying Channel Bonding (CB) techniques. In particular, we overview the ML models proposed by participants (including Artificial Neural Networks, Graph Neural Networks, Random Forest regression, and gradient boosting) and analyze their performance on an open dataset generated using the IEEE 802.11ax-oriented Komondor network simulator. The accuracy achieved by the proposed methods demonstrates the suitability of ML for predicting the performance of WLANs. Moreover, we discuss the importance of abstracting WLAN interactions to achieve better results, and we argue that there is certainly room for improvement in throughput prediction through ML.

### Repository description
This repository contains the LaTeX files and other complementary material used for the article "Machine Learning for Performance Prediction of Channel Bonding in Next-Generation IEEE 802.11 WLANs", submitted to ITU journal. 

You can find more information about the project at the website of the challenge's problem statement [https://www.upf.edu/web/wnrg/ai_challenge](https://www.upf.edu/web/wnrg/ai_challenge)

### Acknowledgements
This  work  has  been  partially  supported  by PGC2018-099959-B-I00 (MCIU/AEI/FEDER,UE) and by the Catalan Government under SGR grant for research support (2017-SGR-11888).

### Contribute

If you want to contribute, please contact to [francisco.wilhelmi@upf.edu](francisco.wilhelmi@upf.edu)